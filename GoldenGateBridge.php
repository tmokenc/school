<html>
    <head>
        <meta charset=utf8>
        <title>Nguyen_Golden Gate Bridge</title>
        
        <style>
            .red {
                color: red;
            }
            
        </style>
    
    </head>
    
    <body>
        <h1 class=red>Golden Gate Bridge</h1>
        
        <?php
            $text = "Most Golden Gate (Zlatá brána) je jedním z nejdelších a nejznámějších visutých mostů na světě. Překonává stejnojmennou úžinu u ústí Sanfranciské zátoky do Tichého oceánu a spojuje tak San Francisco na jižním poloostrově s okresem Marin na severním. Není jen dopravní komunikací, ale i symbolem města San Francisca a turistickou atrakcí. O jeho stavbě bylo rozhodnuto v roce _Rok_ a byla zahájena _DatumRok_. Do provozu byl slavnostně uveden _Uveden_ pro pěší a o den později pro automobily. Tehdy byl nejdelší na světě. 
Při stavbě bylo nutno vzít v úvahu častá zemětřesení v oblasti, ale i bouřlivé přílivy a odlivy. Nejobtížnější byla stavba základů pro jižní mostní pilíř. Pracovalo se z vlečných člunů za přílivu i odlivu. Po dostavení pilířů bylo nutno namontovat 2 drátěná lana. Tato montáž se prováděla z visutých lávek. Stavba si vyžádala _Obeti_ obětí na životech (z toho _PriKolapsu_ při kolapsu lešení 17. února 1937). Stavbu mostu vedl inženýr ……………………………*, jeho poradce byl architekt ………………..* Projekt byl financován dluhopisy ve výši …… miliónů dolarů; v cenách r. 2003 by jeho výstavba stála zhruba 
…….* miliardy. 
Most má rozpětí pilířů …….* m, celková délka ……….* m. Má dvě nosná lana, každé má průměr ……* cm a skládá se ze …………* pramenů. Každý pilíř nese zátěž ……………* tun, kotevní kvádry jsou vystaveny zatížení ………………* tun v tahu. Ocelové pylony se skládají z několika tisíc snýtovaných malých ocelových krabic. Věže dosahují výšky ……………….* m nad mořem. Celé dílo váží ………………* tun a obě věže drží pohromadě _VyskaVaha_ nýtů. Poslední nýt je z _Nyty_ ";

            $Rok = "1930"
            $DatumRok = "5. ledna 1933"; 
            $Uveden = "27. května 1937"
            $Obeti = "11";
            $PriKolapsu = "10" 
            $Tvurci = ""; 
            $Ceny = ""; 
            $Pilire = ""; 
            $LanaPrameny = ""; 
            $Zateze = "";
            $VyskaVaha = "600 000"; 
            $Nyty = "ryzího zlata";
            
            $replace = ["_Rok_", "_DatumRok_", "_Uveden_",  "_Obeti_", "_PriKolapsu_", "_Tvurci_", "_VyskaVaha_", "_Nyty_"];
            $with = [$Rok, $DatumRok, $Uveden, $Obeti, $Tvurci, $Ceny, $Pilire, $LanaPrameny, $Zateze, $VyskaVaha, $Nyty];
            
            echo str_replace($text, $replace, $with);
            
            $img_url = "";
        ?>
    
    </body>
</html>
